# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import FormularioRegistro, FormularioEditar
from .models import Usuario

# Register your models here.
class Administrador(UserAdmin):
    add_form = FormularioRegistro
    form = FormularioEditar
    model = Usuario
    list_display = ['email', 'username', 'nombres', 'apellidos','is_staff']

admin.site.register(Usuario, Administrador)